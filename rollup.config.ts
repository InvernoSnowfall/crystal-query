// rollup.config.js
import commonjs from '@rollup/plugin-commonjs';
import nodeResolve from '@rollup/plugin-node-resolve';
import typescript from '@wessberg/rollup-plugin-ts';

export default {
    input: 'src/index.ts',
    output: {
        name: 'crystal-query',
        dir: 'dist',
        format: 'umd',
    },
    plugins: [typescript(), nodeResolve(), commonjs()],
};
