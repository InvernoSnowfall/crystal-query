import Fuse from "fuse.js";
import Parser from "./parser/Parser";

type FuzzyFunc = (target: string, search: string, negate: boolean) => boolean;
type CompareFunc<T> = <T>(this: { doc: T; fuzzy: FuzzyFunc }) => boolean;

class CrystalQueryClass {
  private exec: CompareFunc<any>;

  constructor(
    queryString: string,
    private readonly opts: {
      fuzzyThreshold: number;
    } = {
      fuzzyThreshold: 0.6,
    }
  ) {
    this.exec = this.buildFunction(queryString);
  }

  public query<T>(docs: T[]): T[] {
    return docs.filter((i) => this.checkDoc(i));
  }

  private fuzzy<T>(obj: T, value: string, search: string, negated: boolean) {
    const fuse = new Fuse([value], {
      includeScore: true,
      threshold: this.opts.fuzzyThreshold,
    }).search(search);

    if (fuse.length >= 1 && fuse[0].score != null) {
      return negated ? false : true;
    }

    return negated ? true : false;
  }

  private checkDoc<T>(doc: T): boolean {
    return this.exec.call({
      doc,
      fuzzy: this.fuzzy.bind(this, doc),
    });
  }

  private buildFunction(queryString: string): CompareFunc<any> {
    const jsString = Parser(queryString).parse();

    // Create and assign the function
    return Function("return " + jsString) as any;
  }
}

export default function CrystalQuery(
  ...args: ConstructorParameters<typeof CrystalQueryClass>
) {
  return new CrystalQueryClass(...args);
}
