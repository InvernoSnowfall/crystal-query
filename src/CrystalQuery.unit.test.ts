import { expect } from "chai";
import CrystalQuery from "./CrystalQuery";
describe("Crystal Query", () => {
  const docs = [
    {
      name: "jeremy",
      age: 10,
    },
    {
      name: "jeremiah",
      age: 10,
    },
    {
      name: "jimmy",
      age: 20,
    },
    {
      name: "anna",
      age: 14,
    },
    {
      name: "susan",
      age: 24,
    },
  ];

  it("simple queries", () => {
    expect(CrystalQuery('name = "jimmy"').query(docs)).to.be.deep.eq(
      docs.filter((i) => i.name === "jimmy")
    );
    expect(CrystalQuery("age > 10").query(docs)).to.be.deep.eq(
      docs.filter((i) => i.age > 10)
    );
  });
  describe("fuzzy queries", () => {
    it("should perform fuzzy searches, bot negated and not", () => {
      expect(CrystalQuery("name:jerem").query(docs)).to.be.deep.eq(
        docs.filter((i) => i.name.indexOf("j") !== -1)
      );
      expect(CrystalQuery("-name:jerem").query(docs)).to.be.deep.eq(
        docs.filter((i) => i.name.indexOf("j") === -1)
      );
    });

    it("should allow changing fuzzy param", () => {
      expect(
        CrystalQuery("name:jerem", { fuzzyThreshold: 0.1 }).query(docs)
      ).to.be.deep.eq(docs.filter((i) => i.name.indexOf("jerem") !== -1));
      expect(
        CrystalQuery("-name:jerem", { fuzzyThreshold: 0.1 }).query(docs)
      ).to.be.deep.eq(docs.filter((i) => i.name.indexOf("jerem") === -1));
    });
  });
});
