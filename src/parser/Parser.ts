import Tokenizer from "../tokenizer/Tokenizer";
import Group from "./tree/Group";

class ParserClass {
  private readonly tokenizer: ReturnType<typeof Tokenizer>;
  private jsString: string = "";
  private finished: boolean = false;

  constructor(private readonly str: string) {
    this.tokenizer = Tokenizer(this.str);
  }

  public parse() {
    if (this.finished) {
      return this.jsString;
    }

    this.jsString = Group(this.tokenizer);
    this.finished = true;

    return this.jsString;
  }
}

export default function Parser(str: string) {
  return new ParserClass(str);
}
