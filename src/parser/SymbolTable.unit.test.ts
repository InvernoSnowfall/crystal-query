import { expect } from 'chai';
import { TokenObject } from '../tokenizer/Tokens';
import { Token } from './../tokenizer/Tokens';
import { SYMBOL_TABLE } from './SymbolTable';

describe('Symbol_Table', () => {
    describe('OperatorMapper()', () => {
        it('should return the write values', () => {
            expect(SYMBOL_TABLE.OperatorMapper(TokenObject(Token.Equal, '=', 0, 1))).to.be.eq(
                SYMBOL_TABLE.Equals
            );

            expect(SYMBOL_TABLE.OperatorMapper(TokenObject(Token.LessThan, '<', 0, 1))).to.be.eq(
                SYMBOL_TABLE.LessThan
            );
            expect(
                SYMBOL_TABLE.OperatorMapper(TokenObject(Token.LessThan, '<', 0, 1), true)
            ).to.be.eq(SYMBOL_TABLE.LessThanEqual);

            expect(SYMBOL_TABLE.OperatorMapper(TokenObject(Token.GreaterThan, '>', 0, 1))).to.be.eq(
                SYMBOL_TABLE.GreaterThan
            );
            expect(
                SYMBOL_TABLE.OperatorMapper(TokenObject(Token.GreaterThan, '>', 0, 1), true)
            ).to.be.eq(SYMBOL_TABLE.GreaterThanEqual);
        });

        it('should error on unknown token', () => {
            expect(() => SYMBOL_TABLE.OperatorMapper(TokenObject(Token.Escape, '', 0, 1))).to.throw(
                /Token .* is not an operator/
            );
        });
    });

    describe('Group()', () => {
        it('should return a grouped js string', () => {
            expect(SYMBOL_TABLE.Group('val')).to.be.eq('(val)');
        });
    });

    describe('FuzzyFunction()', () => {
        it('should return a fuzzy function call', () => {
            const [field, str, negated] = ['field', 'str', false];

            expect(SYMBOL_TABLE.FuzzyFunction(field, str, negated)).to.be.eq(
                `this.fuzzy(${field}, "${str}", ${negated})`
            );

            expect(SYMBOL_TABLE.FuzzyFunction(field, str, true)).to.be.eq(
                `this.fuzzy(${field}, "${str}", true)`
            );
        });
    });

    describe('Negate()', () => {
        it('should return the proper values', () => {
            expect(SYMBOL_TABLE.Negate(false)).to.be.eq('');
            expect(SYMBOL_TABLE.Negate(true)).to.be.eq('!');
        });
    });

    describe('String()', () => {
        it('should return a string', () => {
            expect(SYMBOL_TABLE.String('val')).to.be.eq('"val"');
        });
    });

    describe('ObjectProperty()', () => {
        it('should return a property accessor', () => {
            expect(SYMBOL_TABLE.ObjectProperty('val')).to.be.eq('this.doc.val');
        });
    });

    describe('BooleanSymbol()', () => {
        it('should return a property accessor', () => {
            expect(SYMBOL_TABLE.BooleanSymbol(TokenObject(Token.And, '&', 0, 1))).to.be.eq('&&');
            expect(SYMBOL_TABLE.BooleanSymbol(TokenObject(Token.Or, '|', 0, 1))).to.be.eq('||');
        });

        it('should error on unknown', () => {
            expect(() => SYMBOL_TABLE.BooleanSymbol(TokenObject(Token.Escape, '', 0, 1))).to.throw(
                /Unknown boolean symbol/
            );
        });
    });
});
