import Tokenizer from '../../tokenizer/Tokenizer';
import { Token } from '../../tokenizer/Tokens';
import Group from './Group';

export function Query(tokenizer: ReturnType<typeof Tokenizer>): string {
    tokenizer.consumeWhitespace();

    const group = Group(tokenizer);

    tokenizer.consumeWhitespace();
    tokenizer.expect(Token.EOS);

    return group;
}
