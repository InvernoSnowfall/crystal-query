import { SYMBOL_TABLE } from './../SymbolTable';
/**
 * Represents any value such as word, -word, +word, -10, +10, 10
 * @param args
 */
export default function Property(value: string): string {
    return SYMBOL_TABLE.ObjectProperty(value);
}
