import Tokenizer from '../../../tokenizer/Tokenizer';
import { Token } from '../../../tokenizer/Tokens';
import { SYMBOL_TABLE } from './../../SymbolTable';
import { FuzzyValue } from './FuzzyValue';
import { Identifier } from './Identifier';

/**
 * Represents any expression such as prop:value, -prop:value, +prop:value, prop > 5, -prop < 10, 10 > 50
 * @param args
 */
export default function Expression(tokenizer: ReturnType<typeof Tokenizer>): string {
    tokenizer.consumeWhitespace();

    // Expect an identifier
    const [ident1Negated, type1, ident1] = Identifier(tokenizer);

    tokenizer.consumeWhitespace();

    if (
        tokenizer
            .peek()
            .is(
                Token.Number,
                Token.Word,
                Token.EOS,
                Token.And,
                Token.Or,
                Token.OpenParenthesis,
                Token.CloseParenthesis
            )
    ) {
        return `${ident1} ${SYMBOL_TABLE.Equals} ${
            ident1Negated ? SYMBOL_TABLE.False : SYMBOL_TABLE.True
        }`;
    }

    let includeEquals = false;
    tokenizer.consumeWhitespace();
    const operator = tokenizer.expect(Token.Equal, Token.Colon, Token.GreaterThan, Token.LessThan);

    if (tokenizer.consumeIf(Token.Equal)) {
        includeEquals = true;
    }

    if (operator.is(Token.Colon)) {
        const value = FuzzyValue(tokenizer);
        tokenizer.consumeWhitespace();

        return SYMBOL_TABLE.FuzzyFunction(ident1, value, ident1Negated);
    } else {
        tokenizer.consumeWhitespace();
        const [ident2Negated, type2, ident2] = Identifier(tokenizer);
        tokenizer.consumeWhitespace();

        // If we have two numbers, error out
        if (type2 === Token.Number && type1 === Token.Number) {
            throw tokenizer.error(
                `Invalid syntax comparing two numbers. There must be a property in the expression.`
            );
        }

        const str = `${ident1} ${SYMBOL_TABLE.OperatorMapper(operator, includeEquals)} ${
            ident2Negated && type2 === Token.Word ? SYMBOL_TABLE.FlipSign(ident2) : ident2
        }`;

        return ident1Negated && (type1 === Token.Word || type1 === Token.String)
            ? SYMBOL_TABLE.Negate(true) + SYMBOL_TABLE.Group(str)
            : str;
    }
}
