import { expect } from 'chai';
import Tokenizer from '../../../tokenizer/Tokenizer';
import { FuzzyValue } from './FuzzyValue';

describe('Tokenizer', () => {
    it('should return the JS equivalent for all strings', () => {
        const tests = {
            // Booleans
            '%*7something-t@tate': '%*7something-t@tate',
            'hello)': 'hello',
            'hello(': 'hello',
            'hello another': 'hello',
        };

        (Object.keys(tests) as (keyof typeof tests)[]).forEach((query, index) => {
            expect(
                FuzzyValue(Tokenizer(query)),
                `Expected ${query} (test #${index}) to convert to ${tests[query]}`
            ).to.be.eq(tests[query]);
        });
    });
});
