import Tokenizer from '../../../tokenizer/Tokenizer';
import { Token } from '../../../tokenizer/Tokens';

/**
 * Represents the value after a {Property}:{FuzzyValue}
 *
 * Fuzzy values are unique in that they are any symbol (except whitespace or any parenthesis)
 *
 * Example,
 *
 * &{word}  would typically fail due to the & symbol being reserved, but this syntax is allowed (to simplify usage)
 * {word}& however would not
 * @param tokenizer
 */
export function FuzzyValue(tokenizer: ReturnType<typeof Tokenizer>) {
    let str = '';

    let token = tokenizer.peek();

    let count = 0;
    // Accept anything that is not whitespace, or a parenthesis
    while (
        !token.is(Token.Whitespace, Token.OpenParenthesis, Token.CloseParenthesis, Token.EOS) &&
        count < 10
    ) {
        tokenizer.consume();
        str += token.value;
        token = tokenizer.peek();
        count++;
    }

    return str;
}
