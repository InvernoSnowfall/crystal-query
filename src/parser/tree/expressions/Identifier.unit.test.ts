import { expect } from 'chai';
import Tokenizer from '../../../tokenizer/Tokenizer';
import { Token } from '../../../tokenizer/Tokens';
import { Identifier } from './Identifier';

describe('Identifier', () => {
    it('should return the JS equivalent for all strings', () => {
        const tests = {
            // Booleans
            prop: [false, Token.Word, 'this.doc.prop'],
            '-prop': [true, Token.Word, 'this.doc.prop'],

            '10': [false, Token.Number, '10'],
            '-10': [false, Token.Number, '-10'],
            '+10': [false, Token.Number, '10'],

            '"string1"': [false, Token.String, '"string1"'],
            "'string2'": [false, Token.String, '"string2"'],

            // More than one
            'value && some': [false, Token.Word, 'this.doc.value'],
        };

        (Object.keys(tests) as (keyof typeof tests)[]).forEach((query, index) => {
            expect(Identifier(Tokenizer(query))).to.be.deep.eq(tests[query]);
        });
    });

    it('should error on invalid', () => {
        const tests = {
            // Booleans
            '&': /Expected one of .* but got &/,
            '-&': /Expected one of .* but got &/,
            '- word': /Expected one of .* but got whitespace/,
            '- 10': /Expected one of .* but got whitespace/,
        };

        (Object.keys(tests) as (keyof typeof tests)[]).forEach((query, index) => {
            expect(() => Identifier(Tokenizer(query)), `#${index} query ${query}`).to.be.throw(
                tests[query]
            );
        });
    });

    it('should error on disallowed negation', () => {
        const tests = {
            // Booleans
            '-word': /Unexpected word negation/,
        };

        (Object.keys(tests) as (keyof typeof tests)[]).forEach((query, index) => {
            expect(
                () => Identifier(Tokenizer(query), false),
                `#${index} query ${query}`
            ).to.be.throw(tests[query]);
        });
    });
});
