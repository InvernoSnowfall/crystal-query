import Tokenizer from '../../../tokenizer/Tokenizer';
import { Token } from '../../../tokenizer/Tokens';
import Number from '../Number';
import Property from '../Property';
import StringValue from '../String';

export function Identifier(
    tokenizer: ReturnType<typeof Tokenizer>,
    allowWordNegation: boolean = true
    // negated, token, value
): [boolean, Token.Number | Token.Word | Token.String, string] {
    let value = tokenizer.expect(Token.Word, Token.Number, Token.Plus, Token.Minus, Token.String);

    let negated: boolean = false;
    if (value.is(Token.Minus, Token.Plus)) {
        if (value.is(Token.Minus)) {
            negated = true;
        }
        value = tokenizer.expect(Token.Word, Token.Number, Token.String);
    }

    if (value.is(Token.Word)) {
        if (negated && !allowWordNegation) {
            throw tokenizer.error('Unexpected word negation');
        }
        return [negated, Token.Word, Property(value.value as string)];
    } else if (value.is(Token.String)) {
        return [negated, Token.String, StringValue(value.value as string)];
    } else {
        return [false, Token.Number, Number(value.value as number, negated)];
    }
}
