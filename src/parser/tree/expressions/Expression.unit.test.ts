import { expect } from 'chai';
import Tokenizer from '../../../tokenizer/Tokenizer';
import Expression from './Expression';

describe('Expression', () => {
    it('should return the JS equivalent for all strings', () => {
        const tests = {
            // Booleans
            prop: 'this.doc.prop === true',
            '-prop': 'this.doc.prop === false',

            // Fuzzies
            'prop:value': 'this.fuzzy(this.doc.prop, "value", false)',
            'prop:-value': 'this.fuzzy(this.doc.prop, "-value", false)',
            '-prop:value': 'this.fuzzy(this.doc.prop, "value", true)',
            '-prop:-value': 'this.fuzzy(this.doc.prop, "-value", true)',
            "-prop:'string something'": 'this.fuzzy(this.doc.prop, "string something", true)',
            "prop:-'string something'": 'this.fuzzy(this.doc.prop, "-string something", false)',

            // Some mid syntax checking
            'prop:&testing-another': 'this.fuzzy(this.doc.prop, "&testing-another", false)',
            'prop:value-testing': 'this.fuzzy(this.doc.prop, "value-testing", false)',

            // Less than
            'prop < 5': 'this.doc.prop < 5',
            'prop <= 5': 'this.doc.prop <== 5',
            '-prop<5': '!(this.doc.prop < 5)',
            '-prop<=5': '!(this.doc.prop <== 5)',

            // Greater than
            'prop > 5': 'this.doc.prop > 5',
            'prop >= 5': 'this.doc.prop >== 5',
            '-prop > 5': '!(this.doc.prop > 5)',
            '-prop>=5': '!(this.doc.prop >== 5)',

            // Equals
            'prop = 5': 'this.doc.prop === 5',
            'prop == 5': 'this.doc.prop === 5',
            '-prop = 5': '!(this.doc.prop === 5)',
            '-prop == 5 ': '!(this.doc.prop === 5)',
            'prop == "literal"': 'this.doc.prop === "literal"',
            '-prop == "literal"': '!(this.doc.prop === "literal")',
            '"literal" == prop': '"literal" === this.doc.prop',
            '-"litneg" == prop': '!("litneg" === this.doc.prop)',
            '"literal" == -prop': '"literal" === (this.doc.prop * -1)',

            // Whitespace checking
            ' prop == 5 ': 'this.doc.prop === 5',

            // Negatives
            'prop > -5': 'this.doc.prop > -5',

            // Double props
            'prop1 > prop2': 'this.doc.prop1 > this.doc.prop2',
            'prop1 < -prop2': 'this.doc.prop1 < (this.doc.prop2 * -1)',

            // More than one expressoin
            'prop > 10 & some': 'this.doc.prop > 10',
            'prop another': 'this.doc.prop === true',
            'prop and another': 'this.doc.prop === true',
            'prop & another': 'this.doc.prop === true',
            'prop or another': 'this.doc.prop === true',
            'prop | another': 'this.doc.prop === true',
            'prop 5 > 10': 'this.doc.prop === true',
            'prop (': 'this.doc.prop === true',
        };

        (Object.keys(tests) as (keyof typeof tests)[]).forEach((query, index) => {
            expect(
                Expression(Tokenizer(query)),
                `Expected ${query} (test #${index}) to convert to ${tests[query]}`
            ).to.be.eq(tests[query]);
        });
    });

    it('should error out', () => {
        const tests = {
            // Booleans
            '5 > 5': /Invalid syntax comparing two numbers/,
        };

        (Object.keys(tests) as (keyof typeof tests)[]).forEach((query, index) => {
            expect(
                () => Expression(Tokenizer(query)),
                `Expected ${query} (test #${index}) to error`
            ).to.throw(tests[query]);
        });
    });
});
