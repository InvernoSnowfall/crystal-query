import { expect } from 'chai';
import Property from './Property';

describe('Property', () => {
    it('should return the right value', () => {
        expect(Property('value')).to.be.eq('this.doc.value');
    });
});
