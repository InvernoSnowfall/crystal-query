import { SYMBOL_TABLE } from './../SymbolTable';
export default function Number(value: number, negated: boolean = false) {
    return `${negated ? SYMBOL_TABLE.Minus : ''}${value}`;
}
