import { expect } from 'chai';
import StringValue from './String';

describe('String', () => {
    it('should return the right value', () => {
        expect(StringValue('some value')).to.be.eq('"some value"');
        expect(StringValue('another double check')).to.be.eq('"another double check"');
    });
});
