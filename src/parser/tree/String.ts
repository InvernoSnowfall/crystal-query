import { SYMBOL_TABLE } from './../SymbolTable';
export default function StringValue(value: string) {
    return SYMBOL_TABLE.String(value);
}
