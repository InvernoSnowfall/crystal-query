import { expect } from 'chai';
import Number from './Number';

describe('Number', () => {
    it('should return the right value', () => {
        expect(Number(10)).to.be.eq('10');
        expect(Number(10.12)).to.be.eq('10.12');
        expect(Number(-10)).to.be.eq('-10');
        expect(Number(-10.12)).to.be.eq('-10.12');
    });
});
