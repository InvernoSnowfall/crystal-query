import { expect } from 'chai';
import Tokenizer from '../../tokenizer/Tokenizer';
import { Query } from './Query';

describe('Query', () => {
    it('should group all expressions', () => {
        const tests: Record<string, string> = {
            '( self > 10 and (name and test))':
                '(this.doc.self > 10 && (this.doc.name === true && this.doc.test === true))',
            'self & something > 10': 'this.doc.self === true && this.doc.something > 10',
            '    (    (   testing   ) and  another    )   ':
                '((this.doc.testing === true) && this.doc.another === true)',
        };

        Object.keys(tests).forEach((query, index) => {
            expect(
                Query(Tokenizer(query)),
                `Expected ${query} (test #${index}) to convert to ${tests[query]}`
            ).to.be.eq(tests[query]);
        });
    });
});
