import Tokenizer from '../../tokenizer/Tokenizer';
import { Token } from '../../tokenizer/Tokens';
import { SYMBOL_TABLE } from './../SymbolTable';
import Expression from './expressions/Expression';

export default function Group(tokenizer: ReturnType<typeof Tokenizer>): string {
    tokenizer.consumeWhitespace();
    let openingParenthesis = tokenizer.consumeIf(Token.OpenParenthesis);
    tokenizer.consumeWhitespace();

    let groupStr = '';

    // A group should loop until either EOS or end parenthesis
    while (true) {
        if (tokenizer.peek().is(Token.OpenParenthesis)) {
            groupStr += Group(tokenizer);
        } else {
            groupStr += Expression(tokenizer);
        }

        tokenizer.consumeWhitespace();

        // Now expect an AND, OR, End Paren, or EOS
        const separator = tokenizer.peek();

        // Maybe boolean operator
        if (separator.is(Token.And, Token.Or)) {
            tokenizer.consume();

            // Allow an optional extra char
            tokenizer.consumeIf(separator.token);

            groupStr += ' ' + SYMBOL_TABLE.BooleanSymbol(separator) + ' ';

            tokenizer.consumeWhitespace();
        }
        // if the next symbol is a word, append an AND onto the string
        else if (separator.is(Token.Word)) {
            groupStr += ' ' + SYMBOL_TABLE.And + ' ';
        }
        // Did not run into and or or, so expect the end of group
        else {
            if (openingParenthesis) {
                tokenizer.consumeWhitespace();

                tokenizer.expect(Token.CloseParenthesis);

                tokenizer.consumeWhitespace();
            } else {
                tokenizer.expect(Token.EOS);
            }

            break;
        }
    }

    if (openingParenthesis) {
        return SYMBOL_TABLE.Group(groupStr);
    }

    return groupStr;
}
