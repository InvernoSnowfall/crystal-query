import { expect } from 'chai';
import Tokenizer from '../../tokenizer/Tokenizer';
import Group from './Group';
describe('Group', () => {
    it('should group all expressions', () => {
        const tests: Record<string, string> = {
            '( self > 10 )': '(this.doc.self > 10)',

            '( self > 10 and (name and test))':
                '(this.doc.self > 10 && (this.doc.name === true && this.doc.test === true))',

            'self & something > 10': 'this.doc.self === true && this.doc.something > 10',

            '((testing) and another)': '((this.doc.testing === true) && this.doc.another === true)',

            // Ands
            'val and something': 'this.doc.val === true && this.doc.something === true',
            'val  && something': 'this.doc.val === true && this.doc.something === true',
            'val  & something': 'this.doc.val === true && this.doc.something === true',

            // Ors
            'val  or something': 'this.doc.val === true || this.doc.something === true',
            'val  || something': 'this.doc.val === true || this.doc.something === true',
            'val  | something': 'this.doc.val === true || this.doc.something === true',

            // and-less phrases
            'prop1 prop2:something prop3':
                'this.doc.prop1 === true && this.fuzzy(this.doc.prop2, "something", false) && this.doc.prop3 === true',

            'prop1 prop2 > 10': 'this.doc.prop1 === true && this.doc.prop2 > 10',

            'prop1 || prop2 prop3':
                'this.doc.prop1 === true || this.doc.prop2 === true && this.doc.prop3 === true',

            'age > 10 name:jeremy':
                'this.doc.age > 10 && this.fuzzy(this.doc.name, "jeremy", false)',
        };

        Object.keys(tests).forEach((query, index) => {
            expect(
                Group(Tokenizer(query)),
                `Expected ${query} (test #${index}) to convert to ${tests[query]}`
            ).to.be.eq(tests[query]);
        });
    });

    it('should error', () => {
        const tests: Record<string, RegExp> = {
            '(())': /Expected one of .* but got \)/,
            '(something': /Expected \) but got EOS end of string/,
            'something)': /Expected EOS end of string but got \)/,
        };

        Object.keys(tests).forEach((query, index) => {
            expect(
                () => Group(Tokenizer(query)),
                `Expected ${query} (test #${index}) to error`
            ).to.throw(tests[query]);
        });
    });
});
