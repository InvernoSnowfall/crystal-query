import { expect } from "chai";
import Sinon from "sinon";
import Parser from "./Parser";
import * as group from "./tree/Group";

describe("Parser", () => {
  describe("parse()", () => {
    const query = "name";
    const expected = "this.doc.name === true";

    afterEach(() => {
      Sinon.restore();
    });

    it("should parse the expression", () => {
      expect(Parser(query).parse()).to.eq(expected);
    });

    it("should return cached", () => {
      const stub = Sinon.stub(group, "default").returns(expected);

      const parser = Parser(query);
      expect(parser.parse()).to.eq(expected);
      expect(stub.called).to.be.true;
      stub.resetHistory();

      expect(parser.parse()).to.eq(expected);
      expect(stub.called).to.be.false;
    });
  });
});
