import { Token, TokenReturn, TOKEN_NAMES } from './../tokenizer/Tokens';
export const SYMBOL_TABLE = Object.freeze({
    True: 'true',
    False: 'false',
    Negate: (negate: boolean) => (negate ? '!' : ''),

    OperatorMapper: (token: TokenReturn, equals = false) => {
        switch (token.token) {
            case Token.Equal:
                return SYMBOL_TABLE.Equals;
            case Token.LessThan:
                return equals ? SYMBOL_TABLE.LessThanEqual : SYMBOL_TABLE.LessThan;
            case Token.GreaterThan:
                return equals ? SYMBOL_TABLE.GreaterThanEqual : SYMBOL_TABLE.GreaterThan;
            default:
                throw new Error(`Token ${TOKEN_NAMES[token.token]} is not an operator`);
        }
    },
    Equals: '===',
    NotEquals: '!==',
    GreaterThan: '>',
    GreaterThanEqual: '>==',
    LessThan: '<',
    LessThanEqual: '<==',

    Group: (str: string) => `(${str})`,

    FuzzyFunction: (field: string, str: string, negated: boolean) =>
        `this.fuzzy(${field}, ${SYMBOL_TABLE.String(str)}, ${negated})`,

    String: (str: string) => `"${str}"`,

    ObjectProperty: (str: string) => SYMBOL_TABLE.ObjectVar + SYMBOL_TABLE.PropertySeparator + str,

    FlipSign: (str: string) => SYMBOL_TABLE.Group(`${str} * -1`),

    BooleanSymbol(token: TokenReturn) {
        switch (token.token) {
            case Token.And:
                return SYMBOL_TABLE.And;
            case Token.Or:
                return SYMBOL_TABLE.Or;
            default:
                throw new Error(`Unknown boolean symbol ${token.value}`);
        }
    },

    And: '&&',
    Or: '||',

    Plus: '+',
    Minus: '-',

    ObjectVar: 'this.doc',
    PropertySeparator: '.',
});
