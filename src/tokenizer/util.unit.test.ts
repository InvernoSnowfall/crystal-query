import { expect } from 'chai';
import { buildErrorString, generateAlphabet, getNextWord, isWhitespace, strRepeat } from './util';

type retType = ReturnType<typeof getNextWord>;

function getWord(word: string, end = word.length): retType {
    return {
        value: word,
        allNumbers: false,
        start: 0,
        end: end,
        quoted: false,
        hasDecimal: word.indexOf('.') !== -1,
    };
}

function getQuoted(word: string) {
    const obj = getWord(word.substring(1, word.length - 1).replace(/\\/g, ''));
    obj.quoted = true;
    obj.end = word.length;
    return obj;
}

function getFloat(value: string): retType {
    return {
        value,
        allNumbers: true,
        start: 0,
        end: value.length,
        quoted: false,
        hasDecimal: true,
    };
}

function getInt(value: string): retType {
    return {
        value,
        allNumbers: true,
        start: 0,
        end: value.length,
        quoted: false,
        hasDecimal: false,
    };
}

describe('Tokenizer Utility', () => {
    describe('getNextWord()', () => {
        it('should parse out words properly', () => {
            const tests: Record<string | number, retType> = {
                10: getInt('10'),
                51.213: getFloat('51.213'),
                ' ': getInt(''),

                // Unallowed character tests
                'word=': getWord('word'),
                'word>': getWord('word'),
                '"word>"': getQuoted('"word>"'),
                'something   hello': getWord('something'),
                'esc\\+\\-chars\\>': getWord('esc+-chars>', 14),

                // Other quotes
                '"inside\'here"': getQuoted('"inside\'here"'),
            };

            ["'this is quoted'", '"this is quoted ()-+=><"', '"with \\"escape"'].forEach((i) => {
                tests[i] = getQuoted(i);
            });

            ['someword', 'word_-$@%^*~!./?}{[]'].forEach((i) => {
                tests[i] = getWord(i);
            });

            Object.keys(tests).forEach((str, index) => {
                expect(getNextWord(str + '', 0), `Test #${index}: ${str}`).to.be.deep.eq(
                    tests[str]
                );
            });
        });

        it('should error', () => {
            const tests: Record<string, RegExp> = {
                '"no end quote': /Expected " at position \d+ but got end of string/,
            };

            Object.keys(tests).forEach((str, index) => {
                expect(() => getNextWord(str + '', 0), `Test #${index}: ${str}`).to.throw(
                    tests[str]
                );
            });
        });
    });

    describe('isWhitespace', () => {
        it('should match spaces', () => {
            expect(isWhitespace(' ')).to.be.true;
        });

        it('should match newlines', () => {
            expect(isWhitespace('\n')).to.be.true;
        });

        it('should match tabs', () => {
            expect(isWhitespace('\t')).to.be.true;
        });

        it('should not match characters', () => {
            ['a', '_', '(', '', 'y'].forEach((i) => {
                expect(isWhitespace(i)).to.be.false;
            });
        });
    });

    describe('strRepeat()', () => {
        it('should repeat a string', () => {
            expect(strRepeat('t', 5)).to.be.eq('ttttt');
        });
    });

    describe('buildErrorString()', () => {
        it('should return the right error', () => {
            const str = 'some thing with more than 20 letters in it';
            const pos = 20;

            const part = str.substring(0, 40);
            const bottom = strRepeat(' ', 20) + '^';

            const message = 'Some message here';
            const expected = message + '\n' + part + '\n' + bottom + '\n';

            expect(buildErrorString(message, str, pos)).to.be.eq(expected);
        });
    });

    describe('generateAlphabet', () => {
        it('should return the alphabet', () => {
            expect(generateAlphabet()).to.be.deep.eq('abcdefghijklmnopqrstuvwxyz'.split(''));
        });
    });
});
