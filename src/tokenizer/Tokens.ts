import { getNextWord, isWhitespace } from './util';

export enum Token {
    // Simple tokens
    Minus,
    Plus,
    And,
    Or,
    GreaterThan,
    LessThan,
    Equal,
    OpenParenthesis,
    CloseParenthesis,
    Colon,
    Escape,
    Period,

    // Non simple tokens
    Number,
    Whitespace,
    EOS,
    Word,
    String,
}

export const TOKEN_NAMES: Record<Token, string> = {
    [Token.Minus]: 'minus',
    [Token.Plus]: 'plus',
    [Token.And]: '&, and',
    [Token.Or]: '|, or',
    [Token.GreaterThan]: '>',
    [Token.LessThan]: '<',
    [Token.Equal]: '=',
    [Token.OpenParenthesis]: '(',
    [Token.CloseParenthesis]: ')',
    [Token.Colon]: ':',
    [Token.Escape]: '\\',
    [Token.Period]: '.',
    [Token.Number]: 'number',
    [Token.Whitespace]: 'whitespace',
    [Token.EOS]: 'EOS end of string',
    [Token.Word]: 'word',
    [Token.String]: 'string',
};

// These are tokens that are single character and don't require any knowledge of other tokens
// For example, + is a single token, but and "word" is not because it's numerous letters
const SIMPLE_TOKEN_MAPPINGS: Record<string, Token> = {
    '-': Token.Minus,
    '+': Token.Plus,
    '&': Token.And,
    '|': Token.Or,
    '>': Token.GreaterThan,
    '<': Token.LessThan,
    '=': Token.Equal,
    '(': Token.OpenParenthesis,
    ')': Token.CloseParenthesis,
    ':': Token.Colon,
    '\\': Token.Escape,
    '.': Token.Period,
};

// const ADVANCED_TOKENS: Record<Token, RegExp> = {
//     [Token.Number]: /\d+/,
//     [Token.Whitespace]: /\s+/,
//     [Token.Word]:
// }

export interface TokenReturn {
    token: Token;
    value: string | number;
    start: number;
    end: number;

    is(token: Token, ...others: Token[]): boolean;
}

const TOKEN_OBJ_BASE = {
    is(this: TokenReturn, token: Token, ...others: Token[]): boolean {
        others.push(token);
        return others.indexOf(this.token) !== -1;
    },
};

export function TokenObject(
    token: Token,
    value: string | number,
    start: number,
    end: number
): TokenReturn {
    const base: TokenReturn = Object.create(TOKEN_OBJ_BASE);

    base.token = token;
    base.value = value;
    base.start = start;
    base.end = end;

    return base;
}

export function nextToken(str: string, start: number): TokenReturn {
    if (start >= str.length) {
        return TokenObject(Token.EOS, TOKEN_NAMES[Token.EOS], start, start);
    }

    let pos = start;

    let char = str.charAt(pos);

    // Check if it's a simple token
    if (char in SIMPLE_TOKEN_MAPPINGS) {
        return TokenObject(SIMPLE_TOKEN_MAPPINGS[char], char, start, start + 1);
    }

    // Consume all the whitespace
    if (isWhitespace(char)) {
        while (isWhitespace(char)) {
            char = str.charAt(++pos);
        }

        return TokenObject(Token.Whitespace, TOKEN_NAMES[Token.Whitespace], start, pos);
    }

    // All we have left is a word
    const word = getNextWord(str, pos);

    // If it was quoted, always a string
    if (word.quoted) {
        return TokenObject(Token.String, word.value, word.start, word.end);
    }

    // Otherwise it might be something else
    if (word.allNumbers) {
        return TokenObject(
            Token.Number,
            word.hasDecimal ? parseFloat(word.value) : parseInt(word.value),
            word.start,
            word.end
        );
    } else if (word.value === 'and') {
        return TokenObject(Token.And, 'and', word.start, word.end);
    } else if (word.value === 'or') {
        return TokenObject(Token.Or, 'or', word.start, word.end);
    } else {
        return TokenObject(Token.Word, word.value, word.start, word.end);
    }
}
