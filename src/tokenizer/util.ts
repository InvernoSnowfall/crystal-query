const NUMBERS = new Set([...Array(10).keys()].map((i) => i + ''));
const ALLOWED_CHARS = new Set([
    ...generateAlphabet(),
    ...generateAlphabet().map((i) => i.toUpperCase()),
    '_',
    '-',
    '$',
    '@',
    '%',
    '^',
    '*',
    '~',
    '!',
    '.',
    '#',
    ',',
    '/',
    '?',
    '}',
    ']',
    '{',
    '[',
    ...NUMBERS.values(),
]);
/**
 * Works by starting at the front and going until it hits an end char or the end of the string,
 *
 * End characters are any spaces and a quote without a start
 * @param str
 * @param start
 */
export function getNextWord(
    str: string,
    start: number
): {
    value: string;
    allNumbers: boolean;
    start: number;
    end: number;
    quoted: boolean;
    hasDecimal: boolean;
} {
    let value = '';
    let pos = start;
    let allNumbers = true;
    let hasDecimal = false;
    let quote: string | null = null;

    let escaped = false;

    const getReturn = () => {
        return {
            value,
            allNumbers,
            hasDecimal,
            start,
            end: pos,
            quoted: quote != null,
        };
    };

    const throwError = (msg: string) => {
        throw new Error(buildErrorString(msg, str, pos));
    };

    while (true) {
        // If at end, return
        if (pos >= str.length) {
            if (quote != null) {
                throwError(`Expected ${quote} at position ${pos} but got end of string`);
            }

            return getReturn();
        }

        const char = str.charAt(pos);

        if (char === '\\') {
            escaped = true;
            // Otherwise, check if we start with a quote and it's not esccaped
        } else if ((char === '"' || char === "'") && !escaped) {
            if (quote == null) {
                // We're starting a quoted string
                quote = char;
            } else {
                // We expect the quotes to be the same
                if (quote != null && char == quote) {
                    ++pos; // We tick once more to track this quote
                    // We've now finished our word
                    return getReturn();
                }

                // Otherwise, we accept it
                value += char;
            }
        }
        // We've not hit a quote, so check if we're at a space (another termination)
        else {
            if (isWhitespace(char) && quote == null) {
                // We're not in quote, so end
                return getReturn();
            }
            // If we hit an unallowed char and we're not in a quote, and not escaped, return
            else if (!ALLOWED_CHARS.has(char) && quote == null && !escaped) {
                return getReturn();
            }
            // If we find a decimal, set the flag
            else if (char === '.') {
                hasDecimal = true;
            }
            // If we find a non number, all numbers is false
            else if (!NUMBERS.has(char)) {
                allNumbers = false;
            }

            value += char;
        }
        // Consume any escape
        if (char !== '\\') {
            escaped = false;
        }
        ++pos;
    }
}

export function generateAlphabet() {
    return [...Array(26)].map((_, y) => String.fromCharCode(y + 97));
}

export function isWhitespace(char: string) {
    return char === ' ' || char === '\t' || char === '\n';
}

export function strRepeat(str: string, times: number) {
    return [...Array(times)].map(() => str).join('');
}

/**
 * Builds an array pointing to the problem
 * @param str
 * @param errorPos
 */
export function buildErrorString(msg: string, str: string, pos: number, plusMinus: number = 20) {
    const leftPos = Math.max(0, pos - plusMinus);
    const rightPos = Math.min(str.length - 1, pos + plusMinus);

    return `${msg}
${str.substring(leftPos, rightPos)}
${strRepeat(' ', pos - leftPos)}^\n`;
}
