import { nextToken, Token, TOKEN_NAMES } from './Tokens';
import { buildErrorString } from './util';

class TokenizeClass {
    private position: number = 0;
    private peekValue: ReturnType<typeof nextToken> | null = null;

    constructor(private readonly str: string) {}

    public peek() {
        if (this.peekValue != null) {
            return this.peekValue;
        }

        this.peekValue = nextToken(this.str, this.position);
        return this.peekValue;
    }

    public consume() {
        const val = this.peek();

        this.peekValue = null;
        this.position = val.end;

        return val;
    }

    public consumeWhitespace() {
        return this.consumeIf(Token.Whitespace);
    }

    public consumeIf(token: Token, ...others: Token[]): boolean {
        if (this.peek().is(token, ...others)) {
            this.consume();
            return true;
        }

        return false;
    }

    public expect(token: Token, ...expected: Token[]) {
        expected.push(token);

        const next = this.consume();

        if (expected.indexOf(next.token) === -1) {
            throw this.error(
                `Expected ${expected.length >= 2 ? 'one of ' : ''}${expected
                    .map((i) => TOKEN_NAMES[i])
                    .join(', ')} but got ${TOKEN_NAMES[next.token]}`
            );
        }

        return next;
    }

    public error(msg: string) {
        return new Error(buildErrorString(msg, this.str, this.position));
    }
}

export default function Tokenizer(str: string) {
    return new TokenizeClass(str);
}
