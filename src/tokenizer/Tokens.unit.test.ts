import { expect } from 'chai';
import { nextToken, Token, TokenObject } from './Tokens';

function word(word: string, end = word.length) {
    return TokenObject(Token.Word, word, 0, end);
}

function str(word: string, end = word.length) {
    return TokenObject(Token.String, word, 0, end);
}

describe('Tokens', () => {
    it('should properly return all simple tokens', () => {
        const tests = {
            '+': TokenObject(Token.Plus, '+', 0, 1),
            '-': TokenObject(Token.Minus, '-', 0, 1),
            '&': TokenObject(Token.And, '&', 0, 1),
            '|': TokenObject(Token.Or, '|', 0, 1),
            '>': TokenObject(Token.GreaterThan, '>', 0, 1),
            '<': TokenObject(Token.LessThan, '<', 0, 1),
            '=': TokenObject(Token.Equal, '=', 0, 1),
            '(': TokenObject(Token.OpenParenthesis, '(', 0, 1),
            ')': TokenObject(Token.CloseParenthesis, ')', 0, 1),
            ':': TokenObject(Token.Colon, ':', 0, 1),
            '\\': TokenObject(Token.Escape, '\\', 0, 1),
            '.': TokenObject(Token.Period, '.', 0, 1),
        };

        (Object.keys(tests) as (keyof typeof tests)[]).forEach((str, index) => {
            expect(nextToken(str, 0), `Test ${index}: ${str}`).to.be.deep.eq(tests[str]);
        });
    });

    it('should properly return advanced tokens', () => {
        const words = ["'this\\'issinglescaped'", '"this\\"isescaped"'];
        const tests = {
            '      ': TokenObject(Token.Whitespace, 'whitespace', 0, 6),
            '\t': TokenObject(Token.Whitespace, 'whitespace', 0, 1),
            '\n': TokenObject(Token.Whitespace, 'whitespace', 0, 1),
            10: TokenObject(Token.Number, 10, 0, 2),
            '10.2': TokenObject(Token.Number, 10.2, 0, 4),
            [words[0]]: str("this'issinglescaped", words[0].length),
            [words[1]]: str('this"isescaped', words[1].length),
            thisis3word: word('thisis3word'),
            'this#@*is1a*%-word/.': word('this#@*is1a*%-word/.'),

            '"this is a string"': str('this is a string', 18),
        };

        (Object.keys(tests) as (keyof typeof tests)[]).forEach((str, index) => {
            expect(nextToken(str + '', 0), `Test #${index}: ${str}`).to.be.deep.eq(tests[str]);
        });
    });
});
