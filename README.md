<!-- omit in toc -->
# Crystal Query 

A quick query language I wrote. It's intended to be used in a CLI project I am planning to work on, plus it was fun to write. I am by no means an expert on writing languages, so I most definitely did not follow industry best practices.

This project uses Semantic Versioning.

Current version: `0.0.2`

> ⚠ Because the package is still under heavy development, expect any changes pre v1.0.0 to be potentially breaking changes even if semver doesn't indicate it.

<!-- omit in toc -->
## TOC
- [1. Motivation](#1-motivation)
- [2. Features](#2-features)
  - [2.1. Installation](#21-installation)
  - [2.2. The Language](#22-the-language)
    - [2.2.1. Operators](#221-operators)
    - [2.2.2. Simple Queries](#222-simple-queries)
      - [2.2.2.1. Words/Strings](#2221-wordsstrings)
    - [2.2.3. Logic](#223-logic)
    - [2.2.4. Grouping](#224-grouping)
  - [2.3. Usage](#23-usage)
- [3. Setup](#3-setup)
  - [3.1. Building](#31-building)
  - [3.2. Testing](#32-testing)
- [4. Pipeline](#4-pipeline)
- [5. Limitations](#5-limitations)
- [6. Goals](#6-goals)
- [7. Contribute](#7-contribute)
- [8. License](#8-license)

## 1. Motivation

I wanted a query language that was super simply to use, did not require storing/reading from files, and would operate well with a one-time run as a CLI. I found a few packages on NPM, but I wasn't all too happy with them. So I made this one. Plus, it was fun.

> ⚠ Please note this is not a replacement for languages like Apache Lucene. This does not index data or store it anywhere. 

## 2. Features

-   AND/OR nested logic
-   Simple operators (`>,<,=,<=,>=`)
-   Property based searching (fuzzy search)
-   Negations
-   Boolean searches
-   Only one production dependency

### 2.1. Installation

You first have to setup gitlab to reference `@InvernoSnowfall`. I may eventually publish to the NPM public registry.

```
npm config set @InvernoSnowfall:registry https://gitlab.com/api/v4/packages/npm
```

Use your preferred package manager. For `npm`, you would run

```
npm i @InvernoSnowfall/crystal-query
```

### 2.2. The Language

A query is at it's base just the name of the property followed by operators. Note that a property name by itself is valid.

```
propName {operator {identifier}}
```

#### 2.2.1. Operators

Here is a list of valid operators

* `=`,`==` - equality checker. Both are valid and are the same
* `>`
* `<`
* `>=`
* `<=`
* `-` - A negation when in front of a word, negative symbol in front of numbers
* `+` - Opposite of negation in front of words, position in front of numbers
* `&`, `&&`, `and` - And operator
* `|`, `||`, `or` - Or operator

Furthermore, you can of course use parenthesis to group things, ``(...)``

#### 2.2.2. Simple Queries

Using this example object

```ts
interface Person {
  firstName: string,
  lastName: string,
  age: number,
  houseNum: number,
  zip: number,
  country: string
  alive: boolean // I couldn't come up with something better, okay?
}
```

I can query like so

```
alive     // will filter anyone who is alive
-alive    // Will return anyone who is not alive

firstName:ben            // Will fuzzy search for "ben"
-firstName:ben     // Will fuzzy search for matches that don't have ben
firstName:-ben           // Fuzzy search for "-ben". Note that - is allowed here despite being a special char
country:"United Kingdom" // Use quotes for spaces
firstName:som*e&name     // Bad example, but special characters are allowed and do not need quoted


firstName = "exact"      // I can perform an exact string match

age > 10       // Anyone who is older than 10
age < 50       // less than 50
age <= 30      // less than or equal to 30
age >= 15      // greater than or equal to 15
age = 15       // anyone is age 15
age == 15      // Same as above

// Note the syntax is valid in reverse
15 > age

// I can also compare two properties
zip > houseNum
```

##### 2.2.2.1. Words/Strings

Anything that is not wrapped in quotes or following a colon is considered a property name. Values allowed in property names are pretty much anything besides reserved symbols such as the greater than symbol. Therefore, symbols like `$@%*` are all valid.

*Note*: The exception to this rule is after the `:` symbol. Anything after it is considered valid except for whitespace and either parenthesis `(` and `)`

> ⚠️ I plan to use ^ as a future symbol. Right now it is allowed but I advise against using it.

#### 2.2.3. Logic

I can include simple logic with the ``and`` and ``or`` operators.

Using the same example above,

```
age > 10 & name:jeremy        // Anyone who has a name like jeremy and is older than 10
age > 10 and name:jeremy      // Same as above

age == 50 | zip == 10006      // Anyone who is age 50 or lives in zip code 10006
age = 50 or zip = 10006       // Same as above

// Secondary and
age > 10 name:jeremy    // Functionally the same as the first example
```

#### 2.2.4. Grouping

I can also use parenthesis to group statements


```
age > 10 && (zip > 50000 || name:alex) 
((age < 10 || age > 60) && zip = 10008)
```

### 2.3. Usage

```ts
import CrystalQuery from "@InvernoSnowfall/crystal-query"

// Creates and parses the query
const query = CrystalQuery("name", 
// options
{
  fuzzyThreshold: 0.1 // Cutoff for what constitutes as a match. The lower the number, the less changes must occur for an exact match
});

const docs = [
  //...
]

query.query(docs)

```

## 3. Setup

For this project, `pnpm` is used, so you'll need that before starting. `pnpm`` uses the same CLI commands and options as `npm, so use it normally.

### 3.1. Building

To build, simply run `pnpm run build`.

### 3.2. Testing

The project uses `mocha` and `nyc` for testing and coverage. Just run the `test` script in package.json and the test will run. Reports are stored under `reports/`.

## 4. Pipeline

As a GitLab project, this uses the GitLab CI system. The following jobs occur when

* PR created or merged
  * lint
  * test
* master tagged
  * lint
  * test
  * publish (depends on the following)
    * setup npmrc (required for push)
    * build
  
## 5. Limitations

- Works by converting the query into a JavaScript statement
    - This prevents me from doing anything beyond a simple boolean filter
    - Slight security concern
    - Performance concern
- There is no actual grammar written, sorry :/
- Not designed for heavy indexing or persistence
  
## 6. Goals

- I'd like to implement weighted fields that can be manually set, eg `^5` to give this filter a weight of five
- Finishing support for negative fuzzy filters
- I'd like to eventually convert it to Rust

- Write performance queries
- Write a grammer

## 7. Contribute

I don't know if anyone will be contributing anytime soon so leaving this blank.

## 8. License

This project is licensed under GNU GPLv3.

[TL;DR GNU GPLv3](https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3)) (NOTE: This is not professional legal advice.)  
[LICENSE.txt](./LICENSE.txt)